# Requisitos
nodeJS y npm instalados (en alguna de las últimas versiones)

# Instalación
- Situarse en el directorio del proyecto
```sh
$ cd quinielator-server
```    
- Instalar dependencias con 
```sh
$ npm install
```
- Ejecutar el servidor con
```sh
$ node server.js
```
Accede al front--> http://localhost:8081