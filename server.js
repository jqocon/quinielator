var express = require('express');
var fs = require('fs');
var request = require('request-promise');
var cheerio = require('cheerio');
var bodyParser = require('body-parser');
var moment = require('moment'); //Para manipular fechas
moment.locale('es');
//var cors = require('cors')
var app = express();

app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/angular', express.static(__dirname + '/node_modules/angular/'));
app.use('/bootstrap', express.static(__dirname + '/node_modules/bootstrap/dist/'));
app.use('/ng-table', express.static(__dirname + '/node_modules/ng-table/dist'));

app.use(function (req, res, next) {
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  console.log('Client IP:', ip);
  next();
});

//app.use(cors());

var USER_AGENT = {
    'User-Agent': 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
};

/*
    Scrap soccerway -
    Mantenemos esta función así para tener un ejemplo de Promise en JS
*/
var getSCway = function (scway_data) {

    var round = scway_data.round;
    var competition = scway_data.competition;
    var jornada = scway_data.jornada - 1;

    return new Promise(function(resolve) {
        var info_url = 'http://es.soccerway.com/a/block_competition_matches_summary?';
        var info_params = [
            'block_id=page_competition_1_block_competition_matches_summary_5',
            'callback_params=%7B%22page%22%3A0%2C%22block_service_id%22%3A%22'+
            'competition_summary_block_competitionmatchessummary%22%2C%22'+
            'round_id%22%3A'+round+'%2C%22outgroup%22%3Afalse%2C%22view%22%3A1%2C%22'+
            'competition_id%22%3A'+competition+'%7D',
            'action=changePage',
            'params=%7B%22page%22%3A'+jornada+'%7D'
        ];
        
        //console.log(info_url + info_params.join('&'));
        //Get info from es.soccerway.com
        var request_options = {
            url: info_url + info_params.join('&'),
            json: true,
            headers: USER_AGENT
        };

        request(request_options).then(function(data) {
            var html = data.commands[0].parameters.content;
            var $ = cheerio.load(html);
            resolve($('.matches tbody').html());
        });
    });
}

function parseSCwayInfo(html_scway_info) {
    var $ = cheerio.load(html_scway_info);
    //var $ = cheerio.load(fs.readFileSync('sample.html'));
    var matches = [];

    $('body').children().each(function(i, ele) {

        if ( $(ele).hasClass('timestamp') &&
                $('body').children().eq(i+1).hasClass('timestamp')) {
            //console.log(i + ' - html - ' + $('body').children().eq(i + 3).attr('href'));
            var date = new Date($(ele).attr('data-value') * 1000);
            //console.log(date.getTime());
            var team1 = $('body').children().eq(i + 2).text();
            var team2 = $('body').children().eq(i + 4).text();
            var url = 'http://es.soccerway.com' + $('body').children().eq(i + 3).attr('href');

            var unix_date = date.getTime();
            var moment_date = moment(unix_date);
            var matchInfo = {
                tsdate : unix_date,
                date: moment_date.format("ddd hh:mm A"),
                fromnow: moment_date.fromNow(),
                team1 : team1,
                team2 : team2,
                url : url
            }
            matches.push(matchInfo);
        }
    });

    return matches;
}

function parseBet365Matches(html_bet365_matches) {
    var $ = cheerio.load(!html_bet365_matches ? fs.readFileSync('cupon.html') : html_bet365_matches );

    var matches = [];
    var lastDate = '';

    $('.podEventRow').each(function(i, ele) {
        if($(ele).prev().find('.wideLeftColumn').children().length == 0) {
            var possibleDate = $(ele).prev().find('.wideLeftColumn').length > 0 ? $(ele).prev().find('.wideLeftColumn').text() + new Date().getFullYear() : lastDate;
            if(possibleDate != 'Invalid Date' && possibleDate != lastDate) {
                lastDate = possibleDate;
            }
        }

        var competitors = $(ele).find('.ippg-Market_CompetitorName');
        var fullOdds = $(ele).find('.ippg-Market_Odds');

        var teams = {
            team1 : $(competitors).eq(0).text().trim(),
            team2 : $(competitors).eq(1).text().trim()
        }
        var odds1 = parseFloat($(fullOdds).eq(0).text().trim());
        var oddsX = parseFloat($(fullOdds).eq(1).text().trim());
        var odds2 = parseFloat($(fullOdds).eq(2).text().trim());

        var percent1 = (100/odds1);
        var percentX = (100/oddsX);
        var percent2 = (100/odds2);
        var total = (percent1 + percentX + percent2)/100;

        var odds = {
            odds1 : Math.round(percent1/total),
            oddsX : Math.round(percentX/total),
            odds2 : Math.round(percent2/total)
        }

        var unix_date = new Date(lastDate).getTime();
        var moment_date = moment(unix_date);
        matches.push({
            tsdate : unix_date,
            date: moment_date.format("ddd l"),
            fromnow: moment_date.fromNow(),
            teams : teams,
            odds : odds
        });
    });

    return matches;
}

//Comp scrapping podEventRow
app.get('/scrapSoccerWay', function(req, res) {
    //Soccerway params
    var round = req.query.round;
    var competition = req.query.competition;
    var jornada = req.query.jornada;

    var scway_params = {
        round:round,
        competition:competition,
        jornada:jornada
    };

    console.log('Calling SoccerWay Promise!');
    getSCway(scway_params).then(function(html_scway_info) {
        var scway_json = parseSCwayInfo(html_scway_info);
        res.send(scway_json);
    });
});

//Comp scrapping podEventRow
app.get('/scrapBet365', function(req, res) {
    
    //Bet365 params
    var key = req.query.key;
    var coupon_url = 'https://mobile.bet365.es/V6/sport/coupon/coupon.aspx?key='+key+'&ot=2';

    var request_options = {
        url: coupon_url,
        headers: USER_AGENT
    };

    var matches = [];
    
    console.log('Calling Bet365!');
    
    //Fallback si nos bloquea Bet365
    //res.send( parseBet365Matches(false) );
    
    request(request_options).then(function(html_bet365_matches) {
        matches = parseBet365Matches(html_bet365_matches);
        res.send(matches);
    });
});

app.listen('8081')
console.log('Magic happens on port 8081');
exports = module.exports = app;