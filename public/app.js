"use strict"; 

angular.module('App', ['ngTable']);

angular.module('App').controller('Controller', ['$scope', '$http', 'NgTableParams', '$filter', function($scope, $http, NgTableParams, $filter){
    
    // <Bet365>
    $scope.inputKey = '1-1-13-34343042-2-3-0-0-1-0-0-4100-0-0-1-0-0-0-0-0-0';
    $scope.colorful=true;

    if (typeof(Storage) !== "undefined")
        $scope.bet365data = localStorage.bet365data != undefined ? JSON.parse(localStorage.bet365data) : [];
    else
        $scope.bet365data = [];

    $scope.loadBet365Table = function() {
        console.log('IN loadBet365Table!');
        $scope.bet365TableParams = new NgTableParams({
            sorting: {
                tsdate : 'asc'
            },
            total: $scope.bet365data.length
        }, {
            getData: function($defer, params) {

                //Si estamos filtrando datos no hacemos llamada de red
                if ($scope.bet365data.length > 0) {
                    $defer.resolve( $filter('orderBy')($scope.bet365data, params.orderBy()) );

                } else {
                      $http.get("http://localhost:8081/scrapBet365?key="+$scope.inputKey).then(function(response) {  
                        console.log('Response Items: ' + response.data.length);
                        $scope.bet365data = response.data;

                        if (typeof(Storage) !== "undefined")
                            localStorage.setItem("bet365data", JSON.stringify(response.data));
                        
                        $defer.resolve($scope.bet365data);
                        //Usar esta url y meter el codigo HTML del cupón en el fichero cupon.html si Bet365 bloquea el acceso
                        //"http://localhost:8081/parsecompetition?round="+SCround+"&competition="+SCcompetition+"&jornada="+$scope.inputJornada
                        // Ya no parece necesario el header
                        //headers: {'Access-Control-Allow-Origin': '*'}
                    });

                }
            }
        });
    };
    // <Bet365 />
    
    // <SoccerWay>
    $scope.inputJornada = 4;
    $scope.selectCompetition = "2";
    $scope.competitions = [
        { //1ª División España
            SCround : '41509',
            SCcompetition : '7'
        },
        { //2ª División España
            SCround : '42973',
            SCcompetition : '12'
        },
        { //Champions league
            SCround : '42384',
            SCcompetition : '10'
        }
    ];

    if (typeof(Storage) !== "undefined")
        $scope.soccerWayData = localStorage.soccerWayData != undefined ? JSON.parse(localStorage.soccerWayData) : [];
    else
        $scope.soccerWayData = [];

    $scope.loadSoccerWayTable = function() {
        console.log('IN loadSoccerWayTable! length: ' + $scope.soccerWayData.length);
        $scope.soccerWayTableParams = new NgTableParams({
            sorting: {
                tsdate : 'asc'
            },
            total: $scope.soccerWayData.length
        }, {
            getData: function($defer, params) {
                console.log('params: ' + params.sorting());
                //Si estamos filtrando datos no hacemos llamada de red
                if ($scope.soccerWayData.length > 0) {
                    $defer.resolve( $filter('orderBy')($scope.soccerWayData, params.orderBy()) );

                } else {
                    var SCround = $scope.competitions[$scope.selectCompetition].SCround;
                    var SCcompetition = $scope.competitions[$scope.selectCompetition].SCcompetition;
                    
                    $http.get("http://localhost:8081/scrapSoccerWay?round="+SCround+"&competition="+SCcompetition+"&jornada="+$scope.inputJornada).then(function(response) {
                        console.log('Response Items: ' + response.data.length);
                        $scope.soccerWayData = response.data;

                        if (typeof(Storage) !== "undefined")
                            localStorage.setItem("soccerWayData", JSON.stringify(response.data));

                        $defer.resolve($scope.soccerWayData);
                        //Usar esta url y meter el codigo HTML del cupón en el fichero cupon.html si Bet365 bloquea el acceso
                        //"http://localhost:8081/parsecompetition?round="+SCround+"&competition="+SCcompetition+"&jornada="+$scope.inputJornada
                        // Ya no parece necesario el header
                        //headers: {'Access-Control-Allow-Origin': '*'}
                    });

                }
            }
        });
    };
    // <SoccerWay />
    
    // function to submit the form after all validation has occurred            
    $scope.submitForm = function(isValid) {
        // check to make sure the form is completely valid
        if (isValid) {
            $scope.bet365data = [];
            $scope.soccerWayData = [];
            $scope.loadBet365Table();
            $scope.loadSoccerWayTable();
        }
    };

    $scope.adjustKey = function(possKey) {
        // check to make sure the form is completely valid
        if(possKey != undefined) {
            console.log(possKey);
            var res = possKey.match(/(?:\d|-){15,}/);
            if(res != null && res.length == 1) {
                $scope.inputKey = res[0];
                return;
            }
        }
        $scope.inputKey = '';
    };

    if (typeof(Storage) !== "undefined") {
        if($scope.bet365data.length > 0)
            $scope.loadBet365Table();
        if($scope.soccerWayData.length > 0)
            $scope.loadSoccerWayTable();
    } else {
        // Sorry! No Web Storage support..
    }
    
}]);